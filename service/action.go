package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

// extract field
func extractStringField(fieldList map[string]interface{},
	name string) (string, error) {
	if field, ok := fieldList[name]; ok {
		switch field.(type) {
		case string:
			return field.(string), nil

		default:
			return "", errors.New("bad field type for " +
				name +
				", was expecting string")
		}
	} else {
		return "", errors.New("string field " +
			name +
			" is missing")
	}
}

func extractIntField(fieldList map[string]interface{},
	name string) (int, error) {
	if field, ok := fieldList[name]; ok {
		switch field.(type) {
		case int:
			return field.(int), nil
		case float64:
			return int(field.(float64)), nil

		default:
			return 0, errors.New("bad field type for " +
				name +
				", was expecting int")
		}
	} else {
		return 0, errors.New("int field " +
			name +
			" is missing")
	}
}

func extractBoolField(fieldList map[string]interface{},
	name string) (bool, error) {
	if field, ok := fieldList[name]; ok {
		switch field.(type) {
		case bool:
			return field.(bool), nil

		default:
			return false, errors.New("bad field type for " +
				name +
				", was expecting bool")
		}
	} else {
		return false, errors.New("bool field " +
			name +
			" is missing")
	}
}

// sent once is to be used with renaming requests as a renaming on two objects with the same name
// can lead to incorrect behaviour (unable to ungroup objects)
func (service *Service) sendControllerRequest(controllerList []device.DeviceController,
	method string,
	endpoint string,
	data string,
	isJson bool,
	isSentOnce bool) error {
	for _, controller := range controllerList {
		request, _ := http.NewRequest(method,
			"https://"+
				controller.Controller.Hostname+
				":"+
				strconv.Itoa(controller.Controller.Port)+
				endpoint,
			bytes.NewBuffer([]byte(data)))
		if isJson {
			request.Header.Add("Content-Type",
				"application/json")
		} else {
			request.Header.Add("Content-Type",
				"application/x-www-form-urlencoded")
		}
		if key, err := service.keyManager.GetOneUseKey(); err == nil {
			request.Header.Add(keystore.OneTimeKeyShareHeaderName,
				key.PublicKey)
			if response, err := common.InsecureHTTPClient.Do(request); err == nil {
				content, _ := ioutil.ReadAll(response.Body)
				_ = response.Body.Close()
				if response.StatusCode != http.StatusOK {
					err := make(map[string]string)
					_ = json.Unmarshal(content,
						&err)
					return errors.New("request to controller gives " +
						strconv.Itoa(response.StatusCode) +
						" [" +
						err["error"] +
						"]")
				}
			} else {
				return err
			}
		}
		if isSentOnce {
			break
		}
	}
	return nil
}

func (service *Service) processMessage(message *amqp.Delivery) error {
	// action
	var action orchestration.Action

	// unmarshal message
	if err := json.Unmarshal(message.Body,
		&action); err == nil {
		if err = action.IsValid(); err == nil {
			for _, content := range action.Content {
				fmt.Println(time.Now(), ":", "Type is", content.Type, ",", "name is", action.Name)
				if _type := device.FindCapabilityFromName(content.Type); _type == device.CapabilityTypes {
					return errors.New("action: bad type " +
						content.Type)
				} else {
					// check action
					switch _type {
					case device.CapabilityTypeLight:
						err = service.processLightAction(&content)
						break
					case device.CapabilityTypeOutlet:
						err = service.processOutletAction(&content)
						break
					case device.CapabilityTypeGamepad:
						err = service.processGamepadAction(&content)
						break
					case device.CapabilityTypeTextToSpeech:
						err = service.processTextToSpeechAction(&content)
						break
					case device.CapabilityTypeMusic:
						err = service.processMusicAction(&content)
						break
					case device.CapabilityTypeMoisture:
						break
					case device.CapabilityTypeTemperature:
						break

					default:
						return errors.New("action: bad actuator type " +
							content.Type)
					}
					if err != nil {
						return errors.New("processing: " +
							err.Error())
					}
				}
			}
		} else {
			return errors.New("action check: " +
				err.Error())
		}
	} else {
		return errors.New("json: " +
			err.Error())
	}
	return nil
}

// process light action
func (service *Service) processLightAction(content *orchestration.ActionContent) error {
	for _, action := range content.Action {
		// find controllers associated with name/type
		if controllerList, err := service.FindControllerList(action.Name,
			device.CapabilityTypeLight); err == nil {
			if identifier, ok := orchestration.ActionIdentifierMap[device.CapabilityTypeLight][string(action.Identifier)]; ok {
				switch identifier {
				case orchestration.ActionIdentifierLightOn:
					err = service.sendControllerRequest(controllerList,
						"PUT",
						"/api/v1/controller/lights/on",
						"name="+
							action.Name,
						false,
						false)
					break
				case orchestration.ActionIdentifierLightOff:
					err = service.sendControllerRequest(controllerList,
						"PUT",
						"/api/v1/controller/lights/off",
						"name="+
							action.Name,
						false,
						false)
					break
				case orchestration.ActionIdentifierLightColor:
					if color, err2 := extractStringField(action.FieldList,
						"color"); err == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/lights/color",
							"name="+
								action.Name+
								"&"+
								color,
							false,
							false)
					} else {
						return err2
					}
					break
				case orchestration.ActionIdentifierTest:
					fallthrough
				case orchestration.ActionIdentifierLightBlink:
					err = service.sendControllerRequest(controllerList,
						"PUT",
						"/api/v1/controller/lights/blink",
						"name="+
							action.Name,
						false,
						false)
					break
				case orchestration.ActionIdentifierLightBrightness:
					if brightness, err2 := extractIntField(action.FieldList,
						"brightness"); err == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/lights/brightness",
							"name="+
								action.Name+
								"&brightness="+
								strconv.Itoa(brightness),
							false,
							false)
					} else {
						return err2
					}
					break

				case orchestration.ActionIdentifierLightChangeName:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/lights/name",
							"name="+
								action.Name+
								"&newName="+
								name,
							false,
							true)
					} else {
						return err2
					}
					break

				default:
					return errors.New("not implemented action identifier " +
						string(action.Identifier) +
						" for light")
				}
				if err != nil {
					return err
				}
			} else {
				return errors.New("invalid action identifier " +
					string(action.Identifier) +
					" for light")
			}
		} else {
			return err
		}
	}
	return nil
}

// process light action
func (service *Service) processOutletAction(content *orchestration.ActionContent) error {
	for _, action := range content.Action {
		// find controllers associated with name/type
		if controllerList, err := service.FindControllerList(action.Name,
			device.CapabilityTypeOutlet); err == nil {
			if identifier, ok := orchestration.ActionIdentifierMap[device.CapabilityTypeOutlet][string(action.Identifier)]; ok {
				switch identifier {
				case orchestration.ActionIdentifierOutletOn:
					err = service.sendControllerRequest(controllerList,
						"PUT",
						"/api/v1/controller/outlets/on",
						"name="+
							action.Name,
						false,
						false)
					break
				case orchestration.ActionIdentifierOutletOff:
					err = service.sendControllerRequest(controllerList,
						"PUT",
						"/api/v1/controller/outlets/off",
						"name="+
							action.Name,
						false,
						false)
					break
				case orchestration.ActionIdentifierTest:
					err = service.sendControllerRequest(controllerList,
						"POST",
						"/api/v1/controller/outlets/test",
						"name="+
							action.Name,
						false,
						false)
					break

				default:
					return errors.New("not implemented action identifier " +
						string(action.Identifier) +
						" for outlet")
				}
			} else {
				return errors.New("invalid action identifier " +
					string(action.Identifier) +
					" for outlet")
			}
		} else {
			return err
		}
	}
	return nil
}

// process button action
func (service *Service) processGamepadAction(content *orchestration.ActionContent) error {
	for _, action := range content.Action {
		if controllerList, err := service.FindControllerList(action.Name,
			device.CapabilityTypeGamepad); err == nil {
			if identifier, ok := orchestration.ActionIdentifierMap[device.CapabilityTypeOutlet][string(action.Identifier)]; ok {
				switch identifier {
				case orchestration.ActionIdentifierGamePadChangeName:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/gamepads/name",
							"name="+
								action.Name+
								"&newName="+
								name,
							false,
							true)
					} else {
						return err2
					}
					break

				default:
					return errors.New("not implemented action identifier " +
						string(action.Identifier) +
						" for gamepad")
				}
			} else {
				return errors.New("invalid action identifier " +
					string(action.Identifier) +
					" for gamepad")
			}
		} else {
			return err
		}
	}
	return nil
}

// process text to speech action
func (service *Service) processTextToSpeechAction(content *orchestration.ActionContent) error {
	for _, action := range content.Action {
		if controllerList, err := service.FindControllerList(action.Name,
			device.CapabilityTypeTextToSpeech); err == nil {
			if identifier, ok := orchestration.ActionIdentifierMap[device.CapabilityTypeTextToSpeech][string(action.Identifier)]; ok {
				switch identifier {
				case orchestration.ActionIdentifierTTSTalk:
					if sentence, err2 := extractStringField(action.FieldList,
						"sentence"); err2 == nil {
						if language, err2 := extractStringField(action.FieldList,
							"language"); err2 == nil {
							var volume int
							if volume, err2 = extractIntField(action.FieldList,
								"volume"); err2 != nil {
								volume = 100
							}
							err = service.sendControllerRequest(controllerList,
								"POST",
								"/api/v1/controller/texttospeech",
								"{ \"sentence\": \""+
									sentence+
									"\",\"language\":\""+
									language+
									"\",\"volume\":"+
									strconv.Itoa(volume)+
									"}",
								true,
								false)

						} else {
							return err2
						}
					} else {
						return err2
					}
					break

				case orchestration.ActionIdentifierTTSChangeName:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/name",
							"name="+
								action.Name+
								"&newName="+
								name,
							false,
							true)
					} else {
						return err2
					}
					break

				case orchestration.ActionIdentifierTest:
					err = service.sendControllerRequest(controllerList,
						"POST",
						"/api/v1/controller/texttospeech/test",
						"",
						false,
						false)
					break

				default:
					return errors.New("not implemented action identifier " +
						string(action.Identifier) +
						" for tts")
				}

				if err != nil {
					return err
				}
			} else {
				return errors.New("invalid action identifier " +
					string(action.Identifier) +
					" for tts")
			}
		} else {
			return err
		}
	}
	return nil
}

func (service *Service) processMusicAction(content *orchestration.ActionContent) error {
	for _, action := range content.Action {
		if controllerList, err := service.FindControllerList(action.Name,
			device.CapabilityTypeMusic); err == nil {
			if identifier, ok := orchestration.ActionIdentifierMap[device.CapabilityTypeMusic][string(action.Identifier)]; ok {
				switch identifier {
				case orchestration.ActionIdentifierMusicLoad:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						if url, err2 := extractStringField(action.FieldList,
							"url"); err2 == nil {
							if isMustPlay, err2 := extractBoolField(action.FieldList,
								"isMustPlay"); err2 == nil {
								var volume int
								if volume, err2 = extractIntField(action.FieldList,
									"volume"); err2 != nil {
									volume = 100
								}
								err = service.sendControllerRequest(controllerList,
									"POST",
									"/api/v1/controller/musics/load",
									"{\"name\":\""+
										name+
										"\",\"url\":\""+
										url+
										"\",\"volume\":"+
										strconv.Itoa(volume)+
										",\"isMustPlay\":"+
										common.BoolMap[isMustPlay]+
										"}",
									true,
									false)
							}
						} else {
							return err2
						}
					} else {
						return err2
					}
					break
				case orchestration.ActionIdentifierMusicDelete:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						err = service.sendControllerRequest(controllerList,
							"DELETE",
							"/api/v1/controller/musics?name="+
								name,
							"",
							false,
							false)
					} else {
						return err2
					}
					break
				case orchestration.ActionIdentifierMusicPlay:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						var volume int
						if volume, err2 = extractIntField(action.FieldList,
							"volume"); err2 != nil {
							volume = 100
						}
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/musics/play",
							"name="+
								name+
								"&volume="+
								strconv.Itoa(volume),
							false,
							false)
					} else {
						return err2
					}
					break
				case orchestration.ActionIdentifierMusicPause:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/musics/pause",
							"name="+
								name,
							false,
							false)
					} else {
						return err2
					}
					break
				case orchestration.ActionIdentifierMusicStop:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/musics/stop",
							"name="+
								name,
							false,
							false)
					} else {
						return err2
					}
					break
				case orchestration.ActionIdentifierMusicVolume:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						var volume int
						if volume, err2 = extractIntField(action.FieldList,
							"volume"); err2 != nil {
							volume = 100
						}
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/musics/volume",
							"name="+
								name+
								"&volume="+
								strconv.Itoa(volume),
							false,
							false)
					} else {
						return err2
					}
					break

				case orchestration.ActionIdentifierMusicSetPlayerName:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err2 == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/name",
							"name="+
								name,
							false,
							true)
					} else {
						return err2
					}
					break

				case orchestration.ActionIdentifierTest:
					err = service.sendControllerRequest(controllerList,
						"PUT",
						"/api/v1/controller/musics/test",
						"",
						false,
						false)
					break

				default:
					return errors.New("not implemented action identifier " +
						string(action.Identifier) +
						" for music")
				}
			} else {
				return errors.New("invalid action identifier " +
					string(action.Identifier) +
					" for music")
			}
		} else {
			return err
		}
	}
	return nil
}

func (service *Service) processTemperatureAction(content *orchestration.ActionContent) error {
	for _, action := range content.Action {
		if controllerList, err := service.FindControllerList(action.Name,
			device.CapabilityTypeTemperature); err == nil {
			if identifier, ok := orchestration.ActionIdentifierMap[device.CapabilityTypeTemperature][string(action.Identifier)]; ok {
				switch identifier {
				case orchestration.ActionIdentifierTemperatureChangeName:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/temperature/name",
							"name="+
								action.Name+
								"&newName="+
								name,
							false,
							true)
					} else {
						return err2
					}
					break

				default:
					return errors.New("not implemented action identifier " +
						string(action.Identifier) +
						" for temperature")
				}
			} else {
				return errors.New("invalid action identifier " +
					string(action.Identifier) +
					" for temperature")
			}
		} else {
			return err
		}
	}
	return nil
}

func (service *Service) processMoistureAction(content *orchestration.ActionContent) error {
	for _, action := range content.Action {
		if controllerList, err := service.FindControllerList(action.Name,
			device.CapabilityTypeMoisture); err == nil {
			if identifier, ok := orchestration.ActionIdentifierMap[device.CapabilityTypeMoisture][string(action.Identifier)]; ok {
				switch identifier {
				case orchestration.ActionIdentifierMoistureChangeName:
					if name, err2 := extractStringField(action.FieldList,
						"name"); err == nil {
						err = service.sendControllerRequest(controllerList,
							"PUT",
							"/api/v1/controller/moisture/name",
							"name="+
								action.Name+
								"&newName="+
								name,
							false,
							true)
					} else {
						return err2
					}
					break

				default:
					return errors.New("not implemented action identifier " +
						string(action.Identifier) +
						" for moisture")
				}
			} else {
				return errors.New("invalid action identifier " +
					string(action.Identifier) +
					" for moisture")
			}
		} else {
			return err
		}
	}
	return nil
}
