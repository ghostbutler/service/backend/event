package service

// we do use this library to declare our endpoints
import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// set here your service version
const (
	VersionMajor = 1
	VersionMinor = 0
)

// this is your service structure, which can contains more then the simple common.Service instance
type Service struct {
	// service
	service *common.Service

	// is running?
	isRunning bool

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// rabbit mq controller
	rabbitMQController *rabbit.Controller

	// rabbit mq consumer
	rabbitMQConsumer <-chan amqp.Delivery
}

// this is the function you will use to build your service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate service
	service := &Service{
		configuration: configuration,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceEvent].Name),
		isRunning: true,
	}

	// build rabbit mq connection
	if rabbitMQController, err := rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err == nil {
		service.rabbitMQController = rabbitMQController
	} else {
		return nil, err
	}
	if _, err := service.rabbitMQController.Channel.QueueDeclare(rabbit.ActionQueueName,
		true,
		false,
		false,
		false,
		nil); err != nil {
		return nil, err
	}

	// run update thread
	go service.updateThread()

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceEvent,
		EventAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		configuration.SecurityManager.Hostname,
		service)

	// service is built
	return service, nil
}

// close service
func (service *Service) Close() {
	service.isRunning = false
	service.rabbitMQConsumer = nil
	service.rabbitMQController.Close()
}

// update
func (service *Service) updateThread() {
	for service.isRunning {
		if channel := service.rabbitMQController.Channel; channel != nil {
			var err error
			if service.rabbitMQConsumer, err = channel.Consume(rabbit.ActionQueueName,
				"",
				false,
				false,
				false,
				false,
				nil); err == nil {
				for message := range service.rabbitMQConsumer {
					// persist request
					//mongo

					// execute action
					var err error
					if err = service.processMessage(&message); err == nil {

					} else {
						fmt.Println("Got message:\n",
							string(message.Body))
						fmt.Println("generate error:")
						fmt.Println(err)
					}

					// ack message
					_ = channel.Ack(message.DeliveryTag,
						false)
				}
			} else {
				fmt.Println("failed to pull rabbit message:",
					err)
			}
		}

		// sleep
		time.Sleep(time.Second)
	}
}

// is running?
func (service *Service) IsRunning() bool {
	return service.isRunning
}

// get controller list
func (service *Service) FindControllerList(name string,
	_type device.CapabilityType) ([]device.DeviceController, error) {
	var parameter string
	if name != "" {
		parameter += "name=" +
			name +
			"&"
	}
	parameter = strings.Replace(parameter,
		" ",
		"%20",
		-1)
	parameter += "type=" +
		device.CapabilityTypeName[_type]
	request, _ := http.NewRequest("GET",
		"https://"+
			service.configuration.Directory.Hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
			"/api/v1/directory/find?"+
			parameter,
		nil)
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		content, _ := ioutil.ReadAll(response.Body)
		_ = response.Body.Close()
		if response.StatusCode == http.StatusOK {
			var controllerList []device.DeviceController
			if err := json.Unmarshal(content,
				&controllerList); err == nil {
				return controllerList, nil
			} else {
				return nil, err
			}
		} else {
			return nil, errors.New("directory request failed with code " +
				strconv.Itoa(response.StatusCode))
		}
	} else {
		return nil, err
	}
}
